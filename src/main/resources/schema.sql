/*
drop table userdetails if exists;
CREATE TABLE userdetails (
    id int AUTO_INCREMENT PRIMARY KEY,
    username varchar(300) NOT NULL,
    password VARCHAR(300) NOT NULL,
    active boolean NOT NULL,
    roles TEXT NOT NULL,
    company VARCHAR ,
    department VARCHAR
);

*/
drop table if exists public.userdetails;
CREATE TABLE public."userdetails" (
    id serial PRIMARY KEY,
    username varchar NOT NULL,
    password VARCHAR NOT NULL,
    active boolean NOT NULL,
    roles TEXT NOT NULL 
);

ALTER TABLE public."userdetails" ADD company TEXT;
ALTER TABLE public."userdetails" ADD department TEXT;

INSERT INTO public."userdetails" (id,username,password,active,roles) VALUES
 	(1,'testuser1', 'pass1', true, 'ROLE_USER'),
	(2,'testuser2', 'pass2', true, 'ROLE_USER'),
	(3,'admin', 'admin', true, 'ROLE_ADMIN');


update  public.userdetails set company='rbs'  where id=1;
update public.userdetails set company='hcltechnologies'  where id=2;
update public.userdetails set department='aicoe' where id=1;
update public.userdetails set department='digitalanalytics' where id=2;
update public.userdetails set department='aicoe' where id=3;
update public.userdetails set company='rbs' where id=3;