# demo-spring-security-token
Demonstrate spring security with jwt


# pre-requisites

Following softwares are needed to be installed for environment setup - 
1. Java - version 1.8
2. Apache Maven 3.6
3. Postgres 9.2
4. Eclipse/Spring ToolSuite for Development IDE 


# Configuration - Postgres

For postgres installation steps refer to Installation guide.

Once post-gres is up -
There is a file <b>schema.sql</b> in resources folder comprising of various database/schema commands automatically called in order at application start up.

You may use any SQL Client or PgAdmin to connect to remote Postgres DB with valid connection string/credentials.


# Build Steps
To build the code after checkout/download  -

1. Go to command prompt
2. Change to the directory root of the code and where pom file resides
3. Execute <b>mvn clean install -DskipTests</b>
4. All required dependencies will be downloaded, If there are no errors you will get success message

# Execution

Go to target directory,
 
Excecute java -jar <<replace-with-project-name>>-SNAPSHOT.jar


## Working on Spring-Boot Project

1. Open STS
2. Click File->Import Existing Maven Project and select the project folder with pom file
3. The project will be imported in to workspace
